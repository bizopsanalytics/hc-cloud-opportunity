--domains that landed on each product/s
       with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        group by 1
       ),
       
       hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain  
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )    
        group by 1,2,3
        ),
        land_product as
        ( 
                select  smart_domain, 
                        count(distinct base_product) as land_count
                from    first_product
                group by 1
        )
        select  case 
                        when b.land_count = 1 and a.base_product = 'HipChat' then 1
                        when b.land_count = 1 and a.base_product = 'Confluence' then 2
                        when b.land_count = 1 and a.base_product in ('JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 3
                        when b.land_count = 1 and a.base_product = 'Bitbucket' then 4
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Confluence') then 5
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 6
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Bitbucket') then 7
                        when b.land_count >=3 and b.land_count <> 1 and b.land_count <> 2 and a.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','JIRA Service Desk','JIRA Core','Confluence') then 8
                else 9
                end as lander_group,
                --a.platform, 
                --a.base_product, 
                count(distinct a.smart_domain) as customer_count
        from first_product as a
        left join land_product as b on a.smart_domain = b.smart_domain
        --where b.land_count = 1
        --and   a.base_product = 'HipChat'
        group by 1--,2,3
        order by 1 asc
        
  ;
  
  -- number of products landed on
  
         with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        group by 1
       ),
       
       hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain  
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        first_product as
        (
        select  a.smart_domain, 
                count(distinct b.base_product) as land_count
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
        and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        group by 1
        )
        select  land_count,
                count(distinct smart_domain) as customer_count
        from first_product
        group by 1
        order by 2 desc
        ;
     --investigate actual land combo
        
               with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        group by 1
       ),
       
       hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain  
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform,
                c.min_date
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )    
        group by 1,2,3,4
        )
        select  smart_domain,
                platform, 
                base_product, 
                min_date
        from first_product
        
  ;
  