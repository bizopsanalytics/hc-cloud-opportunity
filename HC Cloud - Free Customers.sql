with all_cloud_paid as
 (--identify the .
        select  
                a.customer_id,
                --a.contact_id,
                a.current_license_id,
                b.base_product,
                b.platform,
                b.name,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_period as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.current_license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.current_period_end_date_id >= 20160630
        --and     c.level in ('Full', 'Starter')  
        and     a.product_id = 171

        )  
        select level, count(distinct sen)
        from all_cloud_paid
        group by 1                      

;
with hc_total as
(
select  --a.current_license_id, b.license_id
        b.edition, 
        b.source_name,
        b.level,
        b.named_users,
        a.current_customer_domain_id as cust_id
from    model.fact_license_period as a
left join model.dim_license as b on a.current_license_id = b.license_id
where   product_id = 171
and     current_period_end_date_id >= 20160630
)
select  level,
        case 
                when named_users is null then 0
                when named_users = 1     then 1
                when named_users <= 10   then 2
                when named_users <= 50   then 3
                when named_users <= 100  then 4
                when named_users > 100   then 5
        else 6   
        end as named_users,
        count(distinct cust_id) as cust_count
from hc_total 
group by 1,2
order by 1,2

;
