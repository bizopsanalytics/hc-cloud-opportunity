
        with bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform = 'Cloud'
        and     base_product in ('HipChat')
        group by 1
 ),
 first_purchase as 
        (
                select  email_domain,
                        min(date) as min_date
                from    public.sale
                group by 1
        ),
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        --and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), 
        all_prod_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain,
                max(b.unit_count) as unit_count
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        --where   a.product_count in (4,5,6,7) --toggle for prod count
        group by 1,2
        ),
         coown_count as
        ( 
                select  smart_domain,
                        customer_id, 
                        count(distinct base_product) as land_count
                from     all_cloud_paid
                where customer_id in (select customer_id from hc_paid)
                group by 1,2
        ),
        coown_prod as
        (
        select  case 
                        -- 1 product only
                        when a.land_count = 1 and b.base_product in ('HipChat') then 1
                        when a.land_count = 1 and b.base_product in ('Confluence') then 2
                        when a.land_count = 1 and b.base_product in ('JIRA') then 3
                        when a.land_count = 1 and b.base_product in ('JIRA Software') then 4
                        when a.land_count = 1 and b.base_product in ('JIRA Service Desk') then 5
                        when a.land_count = 1 and b.base_product in ('JIRA Core') then 6
                        when a.land_count = 1 and b.base_product in ('Bitbucket') then 7
                        -- Hipchat based 2 combos
                        when a.land_count = 2 and b.base_product in ('HipChat','Confluence') then 8 
                        when a.land_count = 2 and b.base_product in ('HipChat','Bitbucket') then 9
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA') then 10
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Software') then 11
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Core') then 12
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Service Desk') then 13
                        --JIRA family and Confluence only
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Confluence') then 14
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Confluence') then 15
                        when a.land_count = 2 and b.base_product in ('JIRA','Confluence') then 16
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Confluence') then 17
                        -- Hipchat based 3 product combos
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','Confluence') then 18
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA') then 19
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Software') then 20  
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Core') then 21
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk') then 22   
                        --Bitbucket based 3 product combos
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA','Confluence') then 23
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Software','Confluence') then 24
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Core','Confluence') then 25
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Service Desk','Confluence') then 26
                        -- 4 product combos
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA','Confluence') then 27
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software','Confluence') then 28
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core','Confluence') then 29
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk','Confluence') then 30                       
                        -- > 4 product combos                 
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','Confluence') then 31
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Software','JIRA Core','Confluence') then 32
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Core','JIRA Service Desk','Confluence') then 33
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'JIRA Software','Confluence') then 34
                        
                else 35
                end as coown_group,
                --a.platform, 
                --a.base_product, 
                a.smart_domain
        from coown_count as a
        left join all_cloud_paid as b on a.smart_domain = b.smart_domain
        
       )
        select  case 
                      when c.product_count in (4,5,6,7)                  then 1
                      when a.unit_count between 50 and 100               then 2
                      when a.unit_count between 101 and 500               then 3
                      when a.unit_count >500                             then 4
                      when d.company_size > 200                          then 5
                      when cast(e.min_date as date) < cast('2012-06-30' as date) then 6
                      when f.coown_group in (18,20,27,28,31,32,34)       then 7
                      when a.unit_count between 11 and 50                then 8
                      when d.company_size <= 200                         then 9
                      when cast(e.min_date as date) > cast('2012-06-30' as date) and cast(e.min_date as date) < cast('2015-06-30' as date) then 10
                      when c.product_count in (2,3)                  then 11
                      else 12   
                end as segment,
                count(distinct a.customer_id) as cust_count,
                sum(b.amount) as total_amount
        from    all_prod_cohort as a
        left join bookings                      as b on a.smart_domain = b.email_domain
        left join product_ownership             as c on a.customer_id = c.customer_id
        left join zone_bizops.customer_size     as d on a.smart_domain = d.email_domain
        left join first_purchase                as e on a.smart_domain = e.email_domain
        left join coown_prod                    as f on a.smart_domain = f.smart_domain
        group by 1
        order by 1
        
        ;

        