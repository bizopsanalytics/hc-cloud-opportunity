-- heatmap for company size by company size
    with bookings as 
         (
                select email_domain,
                       sum(amount) as amount
                from   public.sale
                where  financial_year = 'FY2016'
                and platform = 'Cloud'
                group by 1
         ),
      first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
       -- where   base_product = 'HipChat'
        --and     platform = 'Cloud'
        group by 1
       ),
       hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        company_group as 
        (
          select  case 
                        when b.company_size in(10,50,200) then 200
                        when b.company_size in(500,1000) then 500
                        when b.company_size > 1000 then 1000
                        when b.company_size is null then 0
                        else company_size
                end as company_size, 
                a.customer_id,
                a.smart_domain
        from    hc_paid as a
        left join zone_bizops.customer_size as b on a.smart_domain = b.email_domain
        )
        select  a.company_size,
                a.smart_domain, 
                min(b.min_date) as land_date,
                sum(amount) as amount
        from    company_group as a
        left join first_purchase as b on a.smart_domain = b.email_domain
        left join bookings as c on a.smart_domain = c.email_domain
        group by 1,2
        order by 1           
   
        ;
               --geo 
         with bookings as 
         (
                select email_domain,
                       country,
                       sum(amount) as amount
                from   public.sale
                where  financial_year = 'FY2016'
                and platform = 'Cloud'
                group by 1,2
         ),
           hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
      
        company_group as 
        (
          select  case 
                        when b.company_size in(10,50,200) then 200
                        when b.company_size in(500,1000) then 500
                        when b.company_size > 1000 then 1000
                        when b.company_size is null then 0
                        else company_size
                end as company_size, 
                a.customer_id,
                a.smart_domain
        from    hc_paid as a
        left join zone_bizops.customer_size as b on a.smart_domain = b.email_domain
        )

        select  a.company_size,
                c.subregion,
                count(distinct a.customer_id) as customer_count,
                sum(d.amount) as total_sales
        from    company_group as a
        left join public.license as b on a.smart_domain = b.tech_email_domain
        left join model.dim_region as c on b.tech_country = c.country_name
        left join bookings as d on a.smart_domain = d.email_domain
        where b.tech_country <> 'Unknown'
        and base_product = 'HipChat'
        and b.tech_country = d.country
        group by 1,2
        order by 2