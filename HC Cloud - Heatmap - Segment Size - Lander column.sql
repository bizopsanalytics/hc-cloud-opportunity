

 --User limit cohort for heatmap.
 
 with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        group by 1
       ),
       
 bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform = 'Cloud'
        --and     base_product in ('HipChat')
        group by 1
 ),
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
 first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )    
        group by 1,2,3
        ),
        land_product as
        ( 
                select  smart_domain, 
                        count(distinct base_product) as land_count
                from    first_product
                group by 1
        ),
        land_prod as
        (
        select  case 
                        when b.land_count = 1 and a.base_product = 'HipChat' then 1
                        when b.land_count = 1 and a.base_product = 'Confluence' then 2
                        when b.land_count = 1 and a.base_product in ('JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 3
                        when b.land_count = 1 and a.base_product = 'Bitbucket' then 4
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Confluence') then 5
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 6
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Bitbucket') then 7
                        when b.land_count >=3 and b.land_count <> 1 and b.land_count <> 2 and a.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','JIRA Service Desk','JIRA Core','Confluence') then 8
                else 8
                end as lander_group,
                --a.platform, 
                --a.base_product, 
                a.smart_domain
        from first_product as a
        left join land_product as b on a.smart_domain = b.smart_domain
       ),
       product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ),
        final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain,
                max(b.unit_count) as unit_count
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        --where   a.product_count in (0) --toggle for prod count
        group by 1,2
        )
        select  c.lander_group,
                case 
                        when unit_count <= 10 then 0
                        when unit_count <= 50 then 1
                        when unit_count <= 100 then 2
                        when unit_count > 100 then 3
                else 4
                end as unit_count, 
                count(distinct customer_id) as customer_count,
                sum(b.amount) as total_amount
        from    final_cohort as a
        left join bookings as b on a.smart_domain = b.email_domain
        left join land_prod as c on a.smart_domain = c.smart_domain
        group by 1,2
        order by 1,2
        ;
  
 

 --Product owned cohort for heatmap.
 
 with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        group by 1
       ),
       
 bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform = 'Cloud'
        --and     base_product in ('HipChat')
        group by 1
 ),
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
 first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )    
        group by 1,2,3
        ),
        land_product as
        ( 
                select  smart_domain, 
                        count(distinct base_product) as land_count
                from    first_product
                group by 1
        ),
        land_prod as
        (
        select  case 
                        when b.land_count = 1 and a.base_product = 'HipChat' then 1
                        when b.land_count = 1 and a.base_product = 'Confluence' then 2
                        when b.land_count = 1 and a.base_product in ('JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 3
                        when b.land_count = 1 and a.base_product = 'Bitbucket' then 4
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Confluence') then 5
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 6
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Bitbucket') then 7
                        when b.land_count >=3 and b.land_count <> 1 and b.land_count <> 2 and a.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','JIRA Service Desk','JIRA Core','Confluence') then 8
                else 8
                end as lander_group,
                --a.platform, 
                --a.base_product, 
                a.smart_domain
        from first_product as a
        left join land_product as b on a.smart_domain = b.smart_domain
       ),
       product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                smart_domain,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1,2       
        )
        select  c.lander_group,
                case 
                        when product_count <= 1 then 1
                        when product_count <= 3 then 2
                        when product_count >= 4 then 3                       
                else 4
                end as prod_count, 
                count(distinct customer_id) as customer_count,
                sum(b.amount) as total_amount
        from    product_ownership as a
        left join bookings as b on a.smart_domain = b.email_domain
        left join land_prod as c on a.smart_domain = c.smart_domain
        group by 1,2
        order by 1,2
        ;
        
          
 --value by company size for heatmap
 
 with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        group by 1
       ),
 bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform  = 'Cloud'
        --and     base_product = 'HipChat'
        group by 1
 ),
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
         first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )    
        group by 1,2,3
        ),
        land_product as
        ( 
                select  smart_domain, 
                        count(distinct base_product) as land_count
                from    first_product
                group by 1
        ),
        land_prod as
        (
        select  case 
                        when b.land_count = 1 and a.base_product = 'HipChat' then 1
                        when b.land_count = 1 and a.base_product = 'Confluence' then 2
                        when b.land_count = 1 and a.base_product in ('JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 3
                        when b.land_count = 1 and a.base_product = 'Bitbucket' then 4
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Confluence') then 5
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 6
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Bitbucket') then 7
                        when b.land_count >=3 and b.land_count <> 1 and b.land_count <> 2 and a.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','JIRA Service Desk','JIRA Core','Confluence') then 8
                else 8
                end as lander_group,
                --a.platform, 
                --a.base_product, 
                a.smart_domain
        from first_product as a
        left join land_product as b on a.smart_domain = b.smart_domain
       )
        select  c.lander_group,
                case 
                        when b.company_size in(10,50,200) then 200
                        when b.company_size in(500,1000) then 500
                        when b.company_size > 1000 then 1000
                        when b.company_size is null then 0
                        else company_size
                end as company_size, 
                count(distinct a.customer_id) as customer_count,
                sum(c.amount) as total_sales
        from    hc_paid as a
        left join zone_bizops.customer_size as b on a.smart_domain = b.email_domain
        left join bookings as c on a.smart_domain = c.email_domain
        left join land_prod as c on a.smart_domain = c.smart_domain
        group by 1,2
        order by 1
        ;
        
-- heatmap for time as customer
with 
bookings as 
         (
                select email_domain,
                       sum(amount) as amount
                from   public.sale
                where  financial_year = 'FY2016'
                and platform = 'Cloud'
               -- and base_product = 'HipChat'
                group by 1
         ),
first_purchase as
               (
                select  email_domain,
                        min(date) as min_date
                from    public.sale
               -- where   base_product = 'HipChat'
                --and     platform = 'Cloud'
                group by 1
               ),
                hc_paid as
         (--identify all the HipChat Cloud customers
                select  
                        a.customer_id,
                        a.contact_id,
                        a.license_id,
                        b.base_product,
                        b.platform,
                        c.sen,
                        c.type,
                        c.unit_count,
                        c.billing_period,
                        c.level,
                        c.account_type,
                        d.smart_domain         
                from    model.fact_license_active as a
                join    model.dim_product as b on a.product_id = b.product_id
                join    model.dim_license as c on a.license_id = c.license_id
                join    model.dim_customer as d on a.customer_id = d.customer_id
                where   a.date_id = 20160630
                and     c.level in ('Full', 'Starter')   
                --and     b.base_product = 'HipChat'  
                and     b.platform = 'Cloud'
                ),
         all_cloud_paid as
         (--identify the Cloud products that are owned by the HipChat cloud cohort.
                select  
                        a.customer_id,
                        a.contact_id,
                        a.license_id,
                        b.base_product,
                        b.platform,
                        c.sen,
                        c.type,
                        c.unit_count,
                        c.billing_period,
                        c.level,
                        c.account_type,
                        d.smart_domain         
                from    model.fact_license_active as a
                join    model.dim_product as b on a.product_id = b.product_id
                join    model.dim_license as c on a.license_id = c.license_id
                join    model.dim_customer as d on a.customer_id = d.customer_id
                where   a.date_id = 20160630
                and     c.level in ('Full', 'Starter')  
                and     b.platform = 'Cloud'
                and     b.base_product in (
                                                'JIRA',
                                                'JIRA Core',
                                                'JIRA Software',
                                                'JIRA Service Desk',
                                                'Confluence',
                                                'Bitbucket',
                                                'HipChat'
                                           )     
                ),
product_ownership as
                (--calculate many cloud products are owned per customer.
                select  customer_id,
                        count(distinct base_product) as product_count
                from    all_cloud_paid
                where   customer_id in (select customer_id from hc_paid)
                group by 1
                ), final_cohort as 
                (--final cohort of HC Cloud only owned for further analysis
                select  a.customer_id, 
                        b.smart_domain,
                        a.product_count       
                from    product_ownership as a
                left join hc_paid as b on a.customer_id = b.customer_id
                --where    a.product_count in (4,5,6,7,8) --toggle for prod count
                
                ),
first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform
        from    hc_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )    
        group by 1,2,3
        ),
land_product as
        ( 
                select  smart_domain, 
                        count(distinct base_product) as land_count
                from    first_product
                group by 1
        ),
land_prod as
        (
        select  case 
                        when b.land_count = 1 and a.base_product = 'HipChat' then 1
                        when b.land_count = 1 and a.base_product = 'Confluence' then 2
                        when b.land_count = 1 and a.base_product in ('JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 3
                        when b.land_count = 1 and a.base_product = 'Bitbucket' then 4
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Confluence') then 5
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','JIRA','JIRA Software','JIRA Service Desk','JIRA Core') then 6
                        when b.land_count = 2 and b.land_count <> 1 and a.base_product in ('HipChat','Bitbucket') then 7
                        when b.land_count >=3 and b.land_count <> 1 and b.land_count <> 2 and a.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','JIRA Service Desk','JIRA Core','Confluence') then 8
                else 8
                end as lander_group,
                --a.platform, 
                --a.base_product, 
                a.smart_domain
        from first_product as a
        left join land_product as b on a.smart_domain = b.smart_domain
       ),
                real_final as (
                select a.smart_domain,     
                        case                           
                            when  b.min_date <= (current_date - interval '80' day) - interval '4' Year then 3
                            when  b.min_date <= (current_date - interval '80' day) - interval '1' Year then 2
                            when  b.min_date <= (current_date - interval '80' day)  then 1    
                        else 4
                        end as land_date, 
                        d.lander_group,    
                        sum(c.amount) as total_sales,
                        count(distinct a.smart_domain) as customer_count
                       
                from    hc_paid as a
                left join first_purchase as b on a.smart_domain = b.email_domain
                left join bookings as c on a.smart_domain = c.email_domain
                left join land_prod as d on a.smart_domain = d.smart_domain
                group by 1,2,3
                )
                select  lander_group, 
                        land_date, 
                        sum(total_sales) as total_sales, 
                        sum(customer_count) as customer_count
              from real_final
              group by 1,2
     ;