 -- total cloud sales
 select        
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform = 'Cloud'
        and     base_product in ('HipChat')

 ;

 --Product owned cohort for heatmap.
 
 with bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform = 'Cloud'
        and     base_product in ('HipChat')
        group by 1
 ),
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), 
        final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain,
                max(b.unit_count) as unit_count
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        --where   a.product_count in (0) --toggle for prod count
        group by 1,2
        )
        select  case 
                when unit_count <= 10 then 0
                when unit_count <= 50 then 1
                when unit_count <= 100 then 2
                when unit_count > 100 then 3
                else 4
                end as unit_count, 
                count(distinct customer_id) as customer_count,
                sum(b.amount) as total_amount
        from    final_cohort as a
        left join bookings as b on a.smart_domain = b.email_domain
        group by 1
        order by 1
        ;
  
   
 --value by company size for heatmap
 
 with bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        and     platform  = 'Cloud'
        and     base_product = 'HipChat'
        group by 1
 ),
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        where   a.product_count in (4,5,6,7,8) --toggle for prod count
        )
        select  case 
                        when b.company_size in(10,50,200) then 200
                        when b.company_size in(500,1000) then 500
                        when b.company_size > 1000 then 1000
                        when b.company_size is null then 0
                        else company_size
                end as company_size, 
                count(distinct a.customer_id) as customer_count,
                sum(c.amount) as total_sales
        from    final_cohort as a
        left join zone_bizops.customer_size as b on a.smart_domain = b.email_domain
        left join bookings as c on a.smart_domain = c.email_domain
        group by 1
        order by 1
        ;
        
-- heatmap for time as customer
with bookings as 
         (
                select email_domain,
                       sum(amount) as amount
                from   public.sale
                where  financial_year = 'FY2016'
                and platform = 'Cloud'
                and base_product = 'HipChat'
                group by 1
         ),
first_purchase as
               (
                select  email_domain,
                        min(date) as min_date
                from    public.sale
               -- where   base_product = 'HipChat'
                --and     platform = 'Cloud'
                group by 1
               ),
                hc_paid as
         (--identify all the HipChat Cloud customers
                select  
                        a.customer_id,
                        a.contact_id,
                        a.license_id,
                        b.base_product,
                        b.platform,
                        c.sen,
                        c.type,
                        c.unit_count,
                        c.billing_period,
                        c.level,
                        c.account_type,
                        d.smart_domain         
                from    model.fact_license_active as a
                join    model.dim_product as b on a.product_id = b.product_id
                join    model.dim_license as c on a.license_id = c.license_id
                join    model.dim_customer as d on a.customer_id = d.customer_id
                where   a.date_id = 20160630
                and     c.level in ('Full', 'Starter')   
                and     b.base_product = 'HipChat'  
                and     b.platform = 'Cloud'
                ),
         all_cloud_paid as
         (--identify the Cloud products that are owned by the HipChat cloud cohort.
                select  
                        a.customer_id,
                        a.contact_id,
                        a.license_id,
                        b.base_product,
                        b.platform,
                        c.sen,
                        c.type,
                        c.unit_count,
                        c.billing_period,
                        c.level,
                        c.account_type,
                        d.smart_domain         
                from    model.fact_license_active as a
                join    model.dim_product as b on a.product_id = b.product_id
                join    model.dim_license as c on a.license_id = c.license_id
                join    model.dim_customer as d on a.customer_id = d.customer_id
                where   a.date_id = 20160630
                and     c.level in ('Full', 'Starter')  
                and     b.platform = 'Cloud'
                and     b.base_product in (
                                                'JIRA',
                                                'JIRA Core',
                                                'JIRA Software',
                                                'JIRA Service Desk',
                                                'Confluence',
                                                'Bitbucket',
                                                'HipChat'
                                           )     
                ),
                product_ownership as
                (--calculate many cloud products are owned per customer.
                select  customer_id,
                        count(distinct base_product) as product_count
                from    all_cloud_paid
                where   customer_id in (select customer_id from hc_paid)
                group by 1
                ), final_cohort as 
                (--final cohort of HC Cloud only owned for further analysis
                select  a.customer_id, 
                        b.smart_domain,
                        a.product_count       
                from    product_ownership as a
                left join hc_paid as b on a.customer_id = b.customer_id
                --where    a.product_count in (4,5,6,7,8) --toggle for prod count
                
                ),
                real_final as (
                select smart_domain,
                         case
                                when product_count in (4,5,6,7) then 3
                                when product_count in (2,3) then 2
                                when product_count in (1) then 1
                        else 4
                        end as product_count,       
                        case                           
                            when  b.min_date <= (current_date - interval '80' day) - interval '4' Year then 3
                            when  b.min_date <= (current_date - interval '80' day) - interval '1' Year then 2
                            when  b.min_date <= (current_date - interval '80' day)  then 1    
                        else 4
                        end as land_date,     
                        sum(c.amount) as total_sales,
                        count(distinct smart_domain) as customer_count
                       
                from    final_cohort as a
                left join first_purchase as b on a.smart_domain = b.email_domain
                left join bookings as c on a.smart_domain = c.email_domain
                group by 1,2,3
                )
                select  product_count, 
                        land_date, 
                        sum(total_sales), 
                        sum(customer_count)
              from real_final
              group by 1,2
        
        -- excel work needed to identify
   
     ;
     -- geo bookings
     
with bookings as 
         (
                select email_domain,
                        country,
                       sum(amount) as amount
                from   public.sale
                where  financial_year = 'FY2016'
                and platform = 'Cloud'
                and base_product = 'HipChat'
                group by 1,2
         ),
hc_paid as
         (--identify all the HipChat Cloud customers
                select  
                        a.customer_id,
                        a.contact_id,
                        a.license_id,
                        b.base_product,
                        b.platform,
                        c.sen,
                        c.type,
                        c.unit_count,
                        c.billing_period,
                        c.level,
                        c.account_type,
                        d.smart_domain         
                from    model.fact_license_active as a
                join    model.dim_product as b on a.product_id = b.product_id
                join    model.dim_license as c on a.license_id = c.license_id
                join    model.dim_customer as d on a.customer_id = d.customer_id
                where   a.date_id = 20160630
                and     c.level in ('Full', 'Starter')   
                and     b.base_product = 'HipChat'  
                and     b.platform = 'Cloud'
  ),
          all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        country as
        (
                select a.smart_domain, 
                       a.sen, 
                       b.tech_country
                from    hc_paid as a
                left join public.license as b on a.sen = b.sen
         ),       
                        
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                smart_domain,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where customer_id in (select customer_id from hc_paid)
        group by 1,2
        ), 
        final_cohort as 
        (
        select  a.customer_id, 
                a.smart_domain,
                product_count
        from    product_ownership as a
        --where   a.product_count in (2,3) --toggle for prod count
        )
        select  
                 case
                        when a.product_count in (4,5,6,7) then 3
                        when a.product_count in (2,3) then 2
                        when a.product_count in (1) then 1
                        else 4
                 end as product_count, 
                e.subregion,
                count(distinct a.customer_id) as customer_count,
                sum(d.amount) as total_sales
        from    final_cohort as a
        left join country as b on a.smart_domain = b.smart_domain    
        left join bookings as d on a.smart_domain = d.email_domain
        left join model.dim_region as e on b.tech_country = e.country_name
        group by 1,2
        order by 1,2
        
