
with hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        )--,
        --countries as 
        --(
        select  c.subregion,
                count(distinct a.smart_domain) as country_count
        from hc_paid as a
        left join public.license as b on a.smart_domain = b.tech_email_domain
        join model.dim_region as c on b.tech_country = c.country_name
        where b.tech_country <> 'Unknown'
        group by 1
        order by 2
       
        ;
        