       --MAU
with base as (
select
month
,cast(date_parse(utc_date,'%Y-%m-%d') as date) as utc_date
,product
,parent_sen
,active_users
,new_users
,returning_users
,resurrected_users
,dormant_users
from raw_product.active_instances as a
join model.dim_date as b on (a.utc_date = date_format(b.date_value, '%Y-%m-%d'))
where 1=1
and b.last_day_of_month
and b.calendar_year_month between '2016-06' and '2016-06'
and a.metric = 'mau'
and a.product = 'hipchat'
)
, instances as (
select
a.parent_sen
,cast(date_parse(min(eval_start_date), '%Y-%m-%d') as date) as initial_start_date
from public.license as a
join base as b using (parent_sen)
where a.base_product = 'hipchat'
and a.platform = 'Cloud'
group by 1
 
)
select
a.month
,a.utc_date
,a.parent_sen
,sum(a.active_users) as active_users
,sum(a.new_users) as new_users
,sum(case when date_diff('day', b.initial_start_date, a.utc_date) > 28 then a.new_users else 0 end) as n2e_users
,sum(case when b.initial_start_date is null or date_diff('day', b.initial_start_date, a.utc_date) <= 28 then a.new_users else 0 end) as n2n_users
,sum(a.returning_users) as returning_users
,sum(a.resurrected_users) as resurrected_users
,sum(a.dormant_users) as dormant_users
from base as a
left join instances as b using (parent_sen)
group by 1,2,3
order by 4 desc
;

--MAU for hc specific domain only.

with 
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                --c.parent_sen,
                d.smart_domain,
                concat('gid-',cast(e.id as varchar)) as parent_sen         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        left join raw_hipchat.email_domains as e on d.smart_domain = e.domain
        where   a.date_id = 20160930
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        and     e.id is not null
        ),
base as (
select
month
,cast(date_parse(utc_date,'%Y-%m-%d') as date) as utc_date
,product
,a.parent_sen
,active_users
,new_users
,returning_users
,resurrected_users
,dormant_users
from raw_product.active_instances as a
join model.dim_date as b on (a.utc_date = date_format(b.date_value, '%Y-%m-%d'))
where 1=1
and b.last_day_of_month
and b.calendar_year_month between '2016-01' and '2016-12'
and a.metric = 'mau'
and a.product = 'hipchat'
)
, instances as (
select
a.parent_sen
,cast(date_parse(min(eval_start_date), '%Y-%m-%d') as date) as initial_start_date
from public.license as a
join base as b using (parent_sen)
where a.base_product = 'hipchat'
and a.platform = 'Cloud'
group by 1
 
)
select
a.parent_sen,
a.month
,a.utc_date
,a.product
,sum(a.active_users) as active_users
,sum(a.new_users) as new_users
,sum(case when date_diff('day', b.initial_start_date, a.utc_date) > 28 then a.new_users else 0 end) as n2e_users
,sum(case when b.initial_start_date is null or date_diff('day', b.initial_start_date, a.utc_date) <= 28 then a.new_users else 0 end) as n2n_users
,sum(a.returning_users) as returning_users
,sum(a.resurrected_users) as resurrected_users
,sum(a.dormant_users) as dormant_users
,count(distinct a.parent_sen) as cust_count
from base as a
left join instances as b using (parent_sen)
where a.parent_sen in (select parent_sen from hc_paid)
group by 1,2,3,4
order by 1,2,3,4
;
