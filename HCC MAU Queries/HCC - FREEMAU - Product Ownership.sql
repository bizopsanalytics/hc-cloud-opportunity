--Product owned cohort for heatmap.
 
 with 
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain,
                max(a.product_count) as prod_count,
                max(b.unit_count) as unit_count
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        group by 1,2
        )
        select  prod_count,
                smart_domain
        from    final_cohort as a
        group by 1,2
        order by 1
        ;
  