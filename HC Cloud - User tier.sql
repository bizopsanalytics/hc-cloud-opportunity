--snapshot
with hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        )
        select  case 
                when unit_count <= 10 then 0
                when unit_count <= 50 then 1
                when unit_count <= 100 then 2
                when unit_count <= 500 then 3
                when unit_count <= 1000 then 4
                when unit_count > 1000 then 5
                else 6
                end as unit_count,
               count(distinct customer_id) as customer_count 
        from  hc_paid as a
        group by 1
        order by 1
        
        ;

--most in past year

with hc_paid as
        (
        select  
                a.customer_id,             
                d.smart_domain,
                max(c.unit_count) as unit_count        
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id between 20150630 and 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        group by 1,2    
        )
        select  case 
                when unit_count <= 10 then 0
                when unit_count <= 50 then 1
                when unit_count <= 100 then 2
                when unit_count <= 500 then 3
                when unit_count <= 1000 then 4
                when unit_count > 1000 then 5
                else 6
                end as unit_count,
               count(distinct customer_id) as customer_count 
        from  hc_paid as a
        group by 1
        order by 1
        
        ;