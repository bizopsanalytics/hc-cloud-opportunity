-- high levels for the total segment size.
 
with  all_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        unit_group as 
        (
         select  case 
                when max(unit_count) <= 10 then 0
                when max(unit_count) <= 50 then 1
                when max(unit_count) <= 100 then 2
                 when max(unit_count) <= 500 then 3
                when max(unit_count) > 500 then 4
                else 5
                end as unit_count,
               customer_id,
               smart_domain
        from  all_paid
        group by customer_id, smart_domain
        )
        
        select  a.unit_count, 
                count(distinct a.customer_id) as customer_count
        from    unit_group as a 
        group by 1
        order by 1
        
        ;
  
   
 --by company size for heatmap
 
with 
 all_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        unit_group as 
        (
         select  case 
                when max(unit_count) <= 10 then 0
                when max(unit_count) <= 50 then 1
                when max(unit_count) <= 100 then 2
                 when max(unit_count) <= 500 then 3
                when max(unit_count) > 500 then 4
                else 5
                end as unit_count,
               customer_id,
               smart_domain
        from  all_paid
        group by customer_id, smart_domain
        )
        
        select  case 
                        when b.company_size in(10,50,200) then 200
                        when b.company_size in(500,1000) then 500
                        when b.company_size > 1000 then 1000
                        when b.company_size is null then 0
                        else company_size
                end as company_size, 
                count(distinct a.customer_id) as customer_count
        from    unit_group as a
        left join zone_bizops.customer_size as b on a.smart_domain = b.email_domain      
        where   a.unit_count = 0 --toggle for unit count
        group by 1
        order by 1
       ;
       
-- by time as customer of atlassian
        
     with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
       -- where   base_product = 'HipChat'
        --and     platform = 'Cloud'
        group by 1
       ),
        all_paid as
 (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        unit_group as 
        (
         select  case 
                when max(unit_count) <= 10 then 0
                when max(unit_count) <= 50 then 1
                when max(unit_count) <= 100 then 2
                 when max(unit_count) <= 500 then 3
                when max(unit_count) > 500 then 4
                else 5
                end as unit_count,
               customer_id,
               smart_domain
        from  all_paid
        ),
        real_final as
        (
        select  a.smart_domain, 
                a.unit_count,
                min(b.min_date) as land_date
        from    unit_group as a
        left join first_purchase as b on a.smart_domain = b.email_domain
        --where a.unit_count = 0
        group by 1
        order by 1  
        )
        select land_date, 
               unit_count,
               count(distinct smart_domain) as cust_count 
        from real_final
        group by 1,2
        -- excel work needed to identify
   
   ;
   --geo
with all_paid as
 (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        --and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        unit_group as 
        (
         select  case 
                when max(unit_count) <= 10 then 0
                when max(unit_count) <= 50 then 1
                when max(unit_count) <= 100 then 2
                 when max(unit_count) <= 500 then 3
                when max(unit_count) > 500 then 4
                else 5
                end as unit_count,
               customer_id,
               smart_domain
        from  all_paid
        ),
        real_final as
        (
        select  a.smart_domain, 
                a.customer_id,
                a.unit_count,
                min(b.min_date) as land_date
        from    unit_group as a
        left join first_purchase as b on a.smart_domain = b.email_domain
        --where a.unit_count = 0
        group by 1,2
        order by 1  
        )

        select  unit_count,
                c.subregion,
                count(distinct a.customer_id) as customer_count
        from    real_final as a
        left join public.license as b on a.smart_domain = b.tech_email_domain
        join model.dim_region as c on b.tech_country = c.country_name
        where b.tech_country <> 'Unknown'
        group by 1,2
        order by 2
          
  