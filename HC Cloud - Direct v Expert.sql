    
      --domains that landed via partner
       with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        where   sold_via_partner = true
        and     base_product = 'HipChat'
        and     platform = 'Cloud'
        group by 1
       ),
       
       hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain  
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        )
       
        select  
                count(distinct smart_domain) as customer_count
        from hc_paid
        where smart_domain in (select email_domain from first_purchase)
   