with 
n2ns as
(
        select sen
        from public.sale
        where sale_type = 'New to New'
        and base_product = 'HipChat'
        and platform = 'Cloud'
        and financial_year = 'FY2016'
   )   ,  

all_cloud_paid as
 (--identify the .
        select  
                a.customer_id,
                c.sen,
                d.smart_domain,
                min(a.initial_start_date_id) as min_date         
        from    model.fact_license_period as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.current_license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   c.level in ('Evaluation')  
        and     a.product_id = 171
        group by 1,2,3

        )  
        select count(sen)
        from all_cloud_paid
        where min_date between 20150701 and 20160630
        and sen in (select sen from n2ns)
