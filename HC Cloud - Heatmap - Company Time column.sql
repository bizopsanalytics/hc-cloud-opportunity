
       
-- by time as customer of atlassian
        
     with bookings as 
 (
        select email_domain,
               sum(amount) as amount
        from   public.sale
        where  financial_year = 'FY2016'
        group by 1
 ),
 first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
       -- where   base_product = 'HipChat'
        --and     platform = 'Cloud'
        group by 1
       ),
        hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        unit_group as 
        (
         select  case 
                when unit_count <= 10 then 0
                when unit_count <= 50 then 1
                when unit_count <= 100 then 2
                when unit_count >=101 then 3
                else 4
                end as unit_count,
               customer_id,
               smart_domain
        from  hc_paid
        )
        select  a.smart_domain, 
                case 
                        when d.company_size in(10,50,200) then 200
                        when d.company_size in(500,1000) then 500
                        when d.company_size > 1000 then 1000
                        when d.company_size is null then 0
                        else company_size
                end as company_size, 
                min(b.min_date) as land_date,
                sum(c.amount) as total_sales
        from    hc_paid as a
        left join first_purchase as b on a.smart_domain = b.email_domain
        left join bookings as c on a.smart_domain = c.email_domain
        left join zone_bizops.customer_size as d on a.smart_domain = d.email_domain
        group by 1,2
        order by 1           
        -- excel work needed to identify
   
  ;
    
--geo 
        with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
       -- where   base_product = 'HipChat'
        --and     platform = 'Cloud'
        group by 1
       ),
       all_paid as
 (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        real_final as
        (
                select  a.smart_domain,
                        min(b.min_date) as land_date
                from    all_paid as a
                left join first_purchase as b on a.smart_domain = b.email_domain
                group by 1
       )
        select  c.subregion,
                a.land_date,
                count(distinct a.smart_domain) as customer_count
        from    real_final as a
        left join public.license as b on a.smart_domain = b.tech_email_domain
        left join model.dim_region as c on b.tech_country = c.country_name
        where b.tech_country <> 'Unknown'
        group by 1,2
        order by 2     
  