
-- cloud penetration, may or may not own server/dc version.
with 
customer_size as 
(
        select  email_domain,
                company_size
        from    zone_bizops.customer_size
),
ownership as 
(-- find all customers who owned Confluence Cloud over time.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                b.platform,
                case
                        when d.unit_count = 987654321 then 500
                        else d.unit_count
                        end as unit_count
                ,       
                d.level,
                e.company_size,
                a.date_id
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        left join    customer_size as e on c.smart_domain = e.email_domain
        where   b.base_product = 'HipChat'
        and     b.platform = 'Cloud'
        and     a.date_id > 20120630
)

        select  date_id, 
                count(distinct customer_id) as cust_count,
                sum(unit_count) as seat_count,
                sum(company_size) as employee_count
        from    ownership
        where   date_id in 
                        (
                                20160930,
                                20160630,
                                20160331,
                                20151231,
                                20150930,
                                20150630,
                                20150331,
                                20141231,
                                20140930,
                                20140630
                         )
        group   by 1
        order   by 1
        ;

--company size analysis
-- may or may not own server/dc
with 
customer_size as 
(
        select  email_domain,
                company_size
        from    zone_bizops.customer_size
),
ownership as 
(-- find all customers who owned Confluence Cloud over time.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                b.platform,
                case
                        when d.unit_count = 987654321 then 500
                        else d.unit_count
                        end as unit_count
                ,       
                d.level,
                e.company_size,
                a.date_id
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        left join    customer_size as e on c.smart_domain = e.email_domain
        where   b.base_product = 'HipChat'
        and     b.platform = 'Cloud'
        and     a.date_id > 20120630
)

        select  date_id, 
                case 
                        when company_size = 10 then 10
                        when company_size = 50 then 50
                        when company_size = 200 then 200
                        when company_size = 500 then 500
                        when company_size > 500 then 501
                else 0
                end as company_size,
                count(distinct customer_id) as cust_count,
                sum(unit_count) as seat_count,
                sum(company_size) as employee_count
        from    ownership
        where   date_id in 
                        (
                                20160930,
                                20160630,
                                20160331,
                                20151231,
                                20150930,
                                20150630,
                                20150331,
                                20141231,
                                20140930,
                                20140630
                         )
        group   by 1,2
        order   by 1,2
        ;
        
--industry Analysis   
with 
customer_size as 
(
        select  email_domain,
                industries,
                case 
                when company_size is null then 10
                else company_size
                end as company_size
        from    zone_bizops.customer_size
),
ownership as 
(-- find all customers who owned HC Cloud over time.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                b.platform,
                case
                        when d.unit_count = 987654321 then 500
                        else d.unit_count
                        end as unit_count
                ,       
                d.level,
                e.company_size,
                e.industries,
                a.date_id
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        left join    customer_size as e on c.smart_domain = e.email_domain
        where   b.base_product = 'HipChat'
        and     b.platform = 'Cloud'
        and     a.date_id > 20120630
)

        select  date_id, 
                industries,
                count(distinct customer_id) as cust_count,
                sum(unit_count) as seat_count,
                sum(company_size) as employee_count
        from    ownership
        where   date_id in 
                        (
                                20160930,
                                20160630,
                                20160331,
                                20151231,
                                20150930,
                                20150630,
                                20150331,
                                20141231,
                                20140930,
                                20140630
                         )
        group   by 1,2
        order   by 1,2
        ;       
                 
--cloud only      
with 
customer_size as 
(
        select  email_domain,
                case 
                when company_size is null then 10
                else company_size
                end as company_size
        from    zone_bizops.customer_size
),
ownership as 
(-- find all customers who owned Confluence Cloud over time.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                b.platform,
                case
                        when d.unit_count = 987654321 then 20000
                        else d.unit_count
                        end as unit_count
                ,       
                d.level,
                e.company_size,
                a.date_id
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        left join    customer_size as e on c.smart_domain = e.email_domain
        where   b.base_product = 'Confluence'
        and     b.platform = 'Cloud'
        and     a.customer_id not in 
                        (
                        select  a.customer_id                       
                        from    model.fact_license_active as a
                        join    model.dim_product as b on a.product_id = b.product_id
                        join    model.dim_customer as c on a.customer_id = c.customer_id
                        where   b.base_product = 'Confluence'
                        and     b.platform in ('Server','Data Center')
                        and     a.date_id = 20160630
                        )
        and     a.date_id > 20120630
)

        select  date_id, 
                count(distinct customer_id) as cust_count,
                sum(unit_count) as seat_count,
                sum(company_size) as employee_count
        from    ownership
        where   date_id in 
                        (
                                20160630,
                                20160331,
                                20151231,
                                20150930,
                                20150630,
                                20150331,
                                20141231,
                                20140930,
                                20140630
                         )
        group   by 1
        order   by 1
        ;                 
        
        
--server DC only
with 
customer_size as 
(
        select  email_domain,
                case 
                when company_size is null then 10
                else company_size
                end as company_size
        from    zone_bizops.customer_size
),
ownership as 
(-- find all customers who owned Confluence Server/DC over time.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                b.platform,
                case
                        when d.unit_count = 987654321 then 20000
                        else d.unit_count
                        end as unit_count
                ,       
                d.level,
                e.company_size,
                a.date_id
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        left join    customer_size as e on c.smart_domain = e.email_domain
        where   b.base_product = 'Confluence'
        and     b.platform in ('Server','Data Center')
        and     a.customer_id not in 
                        (
                        select  a.customer_id                       
                        from    model.fact_license_active as a
                        join    model.dim_product as b on a.product_id = b.product_id
                        join    model.dim_customer as c on a.customer_id = c.customer_id
                        where   b.base_product = 'Confluence'
                        and     b.platform in ('Cloud')
                        and     a.date_id = 20160630
                        )
        and     a.date_id > 20120630
)

        select  date_id, 
                count(distinct customer_id) as cust_count,
                sum(unit_count) as seat_count,
                sum(company_size) as employee_count
        from    ownership
        where   date_id in 
                        (
                                20160630,
                                20160331,
                                20151231,
                                20150930,
                                20150630,
                                20150331,
                                20141231,
                                20140930,
                                20140630
                         )
        group   by 1
        order   by 1
        ;             