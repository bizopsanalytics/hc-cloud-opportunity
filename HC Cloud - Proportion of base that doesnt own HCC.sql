 --to find proportion of cloud base that does not own HC Cloud
        with hc_full_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        hc_starter_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Starter') --('Full'), 'Starter')  -- toggle for starter or full only
        --and     b.platform  = 'Cloud'           --toggle for cloud customer base only
        )


          -- Full or starter only (need to remove starter from all_paid and change platform)
        select  count(distinct customer_id)
        from    all_paid
        where   customer_id not in (select customer_id from hc_full_paid)
        and     customer_id not in (select customer_id from hc_starter_paid)  
        
                -- HC Cloud Starter + Full
        select  count(distinct a.customer_id)
        from    all_paid as a
        join hc_full_paid as b on a.customer_id = b.customer_id
        join hc_starter_paid as c on a.customer_id = c.customer_id
        
             -- starter HC but not full
        select  count(distinct customer_id)
        from    all_paid
        where   customer_id not in (select customer_id from hc_full_paid)
        and     customer_id in (select customer_id from hc_starter_paid)
        
           -- full HC but not starter
        select  count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from hc_full_paid)
        and     customer_id not in (select customer_id from hc_starter_paid)
        ;
        
     